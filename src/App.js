//import logo from './logo.svg';
import './App.css';
import {Routes,Route} from 'react-router-dom';

import BookList from './components/BookList';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import Favourites from './components/Favourites';
import Login from './components/Login';
import PageNotFound from './Page/PageNotFound';



function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<BookList />}/>
        <Route path="/favourites" element={<Favourites />}/>
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
      <Footer title="@ KORCA HUB" />
    </div>
  );
}

export default App;
