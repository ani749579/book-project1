import React from "react";
import { NavLink } from "react-router-dom";

const PageNotFound = () => (
    <div className="page-not-found">
        <div className="text-center" id="page-notfound">
            <h3>Page Not Found!</h3>
            <h2>404</h2>
            <NavLink to="/">Go to home!</NavLink>
        </div>
    </div>
);

export default PageNotFound;