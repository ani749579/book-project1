import React from "react";
import propTypes from "prop-types";

const Footer = (props) => {
    const { title } = props;
    return(
        <div className="footer">
            <div>{title}</div>
        </div>
    )
}

Footer.propTypes = {
    title: propTypes.string,
};

Footer.defaultProps = {
    title: "@ Books app"
};

export default Footer;