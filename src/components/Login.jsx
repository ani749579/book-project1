import React, { useState } from "react";


const Login = (props) => {
    const [data, setData] = useState({
        username: "",
        email: "",
        password: "",
        checkPassword: false
    });

    const onChange = (event) => {
        if (event.target.name === "checkPassword") {
            setData({ ...data, "checkPassword": !data.checkPassword });
        } else {
            setData({ ...data, [event.target.name]: event.target.value });
        }
    }

    const submitForm = (event) => {
        event.preventDefault();
        // console.log("Click");
    }

    return (
        <div className="Login">
          
            <form onSubmit={submitForm}>
                <h2>Login</h2>
                <div className="input">
                    <label htmlFor="username">Username</label>
                    <input
                        placeholder="Username"
                        id="username"
                        name="username"
                        onChange={onChange}
                        value={data.username}
                    />
                </div>
                <div className="input">
                    <label htmlFor="email">Email</label>
                    <input
                        placeholder="Email"
                        id="Email"
                        name="Email"
                        onChange={onChange}
                        value={data.email}
                    />
                </div>
                <div className="input">
                    <label htmlFor="password">Password</label>
                    <input
                        placeholder="Password"
                        type={data.checkPassword ? "text" : "password"}
                        id="password"
                        name="password"
                        onChange={onChange}
                        value={data.password}
                    />
                </div>
                <div className="">
                    <label htmlFor="checkPassword">Show password!</label>
                    <input
                        type="checkbox"
                        id="checkPassword"
                        name="checkPassword"
                        onChange={onChange}
                        value={data.checkPassword}
                    />
                </div>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
};

export default Login;