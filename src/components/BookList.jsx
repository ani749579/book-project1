import React,{useState,useEffect} from "react";
// import { API_URL } from "../API";
import axios from 'axios';
// Vendi apo fajlli ku i ruajm linqet sekrete
const API_URL = process.env.REACT_APP_API_URL;


const BookList =()=>{

  const [books, setBooks]= useState([]);
  console.log("API_URL", API_URL);
  useEffect(()=>{
    
    return () => {
      axios.get(API_URL).then(res =>{
        console.log(res.data)
        setBooks(res.data)
      })
    };
  
    // Kur i kem [] bosh ateher ky useEffect thirr vetem nje here 
  },[]);

    return(
      <div className="book-list">
        {books.map((book)=>(
            <div key={book.id} className="book">
                <div><h3>{book.title}</h3></div>
                <div><h2>{book.authors}</h2></div>
                <div><img src={book.image_url} alt="#"/></div>
                <div><p>{book.Quote1}</p></div>
                <div><button>Add to favourites</button></div>
            </div>
        ))}
      </div>
       
    )
}
export default BookList;