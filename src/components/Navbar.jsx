import React from "react";
import { NavLink } from "react-router-dom";


const Navbar =()=>{
    return(
        <div className="navbar">
            <div><h1>Book App</h1></div>
            <div className="list">
               <NavLink to="/">Home</NavLink> 
            </div>
            <div className="list">
                <NavLink to="/favourites">Favourites</NavLink> 
            </div>
            <div className="list">
                <NavLink to="/test">Test</NavLink> 
            </div>
            <div className="list">
                <NavLink to="/hello">Hello</NavLink> 
            </div>
            <div className="list">
                <NavLink to="/login">Login</NavLink> 
            </div>
        </div>
    )
}
export default Navbar;